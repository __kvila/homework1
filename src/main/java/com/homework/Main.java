package main.java.com.homework;

import java.util.Scanner;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        //Условные операторы
        //1
        //getSumOrMultipli();
        //2
        //getQuarter();
        //3
        //positiveSum();
        //4
        //getMaxExpression(2,2,3);
        //5
        //getStudentRate((byte)100);
        //Циклы
        //1
        //System.out.println(getSumAndQuantity());
        //2
        //checkPrimeNumber(19);
        //3
        //numbRoot(637);
        //4
        //getFactorial(8);
        //5
        //sumNumber(1005);
        //6
        //showMirror(1234);
        //Одномерные массивы
        //1
        //findMin(10);
        //2
        //findMax(10);
        //3
        //findMinIndex(10);
        //4
        //findMaxIndex(10);
        //5
        //sumOddElem(5);
        //6
        //reverseArr(10);
        //7
        //getOddNumsQuantity(10);
        //8
        //swapHalves(12);
        //9.1
        //sortBubble(5);
        //9.2
        //sortSelect(5);
        //9.3
        //sortInsert(5);
        //Функции
        //1
        //getDay(7);
        //2
        //getDotDist(1,1, 2, 2);
        //3
        //getStr(1000);
        //4
        //getNumFromStr();
    }

    public static String getStr(int num)
    {
        if(num<0 || num>999)
        {
            return "Invalid number! Enter a number between 0 and 999...";
        }

        String [] num0_9 = {"ноль","один","два","три","четыре","пять","шесть","семь","восемь","девять"};
        String [] num10_19 = {"десять","одиннадцать","двенадцать","тринадцать","четырнадцать","пятнадцать","шестнадцать","семнадцать","восемнадцать","девятнадцать"};
        String [] num20_90 = {"двадцать","тридцать","сорок","пятьдесят","шестьдесят","семьдесят","восемьдесят","девяносто"};
        String [] num100_900 = {"сто","двести","триста","четыреста","пятьсот","шестьсот","семьсот","восемьсот","девятьсот"};

         String str="";
         int count;


             if(num/100>=1)
             {
                 count = num / 100;
                 num -= count * 100;
                 str+=num100_900[count-1]+" ";
                 if(num==0) {
                     return str;
                 }
             }

            if(num>9 && num<20)
            {
                count = num;
                num = 0;
                str+=num10_19[count-10]+" ";
                return str;
            }

        if(num>19)
        {
                    count = num / 10;
                    num -= count * 10;
                    str+=num20_90[count-2]+" ";
                    if(num==0)
                    {
                        return str;
                    }
        }

        if(num<10)
        str+=num0_9[num];

        return str;
    }

    public static String getDotDist(int x1, int y1, int x2, int y2)
    {
        return "Distance between two dots is: " + (Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2)))/2;
    }

    public static String getDay(int day)
    {
        if(day<1 || day>7) {
            return "Enter a number between 1 and 7";
        }
        if(day==1)
            return "Monday";
        else
            if(day==2)
                return "Tuesday";
            else
            if(day==3)
                return "Wednesday";
            else
            if(day==4)
                return "Thursday";
            else
            if(day==5)
                return "Friday";
            else
            if(day==6)
                return "Saturday";
            else
                return "Sunday";
    }

    public static String sortInsert(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int buff = 0;
        String strArr = "";

        for(int i=1;i<arr.length;i++)
            for(int j=i;j>0 && arr[j-1]>arr[j];j--)
            {
                buff = arr[j-1];
                arr[j-1] = arr[j];
                arr[j]=buff;
            }

        for(int i = 0; i<arr.length; i++)
        {
            strArr += arr[i] + " ";
        }

        return strArr;
    }

    public static String sortSelect(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int buff = 0;
        String strArr = "";

        int nSize;
        buff = 0;
        for(int i=1;i< arr.length;i++) {
            int max = arr[0], maxIndex = 0;
            nSize = arr.length-i;
            for (int j = 1; j < nSize+1; j++) {
                if (arr[j] > max) {
                    max = arr[j];
                    maxIndex = j;
                }
            }
            buff = arr[arr.length-i];
            arr[arr.length-i] = arr[maxIndex];
            arr[maxIndex]=buff;
        }

        for(int i = 0; i<arr.length; i++)
        {
            strArr += arr[i] + " ";
        }

        return strArr;
    }

    public static String sortBubble(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int buff = 0;
        String strArr = "";

        for(int i=0;i<arr.length-1;i++)
            for(int j=arr.length-1;j>i;j--)
            {
                if(arr[j]<arr[j-1])
                {
                    buff = arr[j];
                    arr[j]=arr[j-1];
                    arr[j-1]=buff;
                }
            }

        for(int i = 0; i<arr.length; i++)
        {
            strArr += arr[i] + " ";
        }

        return strArr;
    }

    public static String swapHalves(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        String strArr = "";

        for (int i = 0; Math.round(i+arr.length/2) <= arr.length-1; i++) {
            if(arr.length%2!=0) {
                if (arr.length == Math.round(i + arr.length / 2 + 1))
                    break;
                int tmp = arr[i];
                arr[i] = arr[Math.round(i + arr.length / 2 + 1)];
                arr[Math.round(i + arr.length / 2) + 1] = tmp;
            }
            else
            {
                int tmp = arr[i];
                arr[i] = arr[Math.round(i + arr.length / 2)];
                arr[Math.round(i + arr.length / 2)] = tmp;
            }
        }

        for(int i = 0; i<arr.length; i++)
        {
            strArr += arr[i] + " ";
        }

        return strArr;
    }

    public static int getOddNumsQuantity(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int quant=0;
        for(int i = 0;i< arr.length;i++)
        {
            if(arr[i]%2!=0)
                quant++;
        }
        return quant;
    }

    public static String reverseArr(int el1, int el2, int el3, int el4, int el5)
    {
        String strArr = "";
        int [] arr = new int[]{el1, el2, el3, el4, el5};

        for (int i = 0; i < arr.length / 2; i++) {
            int tmp = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = tmp;
        }

         for(int i = 0; i<arr.length; i++)
        {
            strArr += arr[i] + " ";
        }

        return strArr;
    }

    public static int sumOddElem(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int sum=0;
        for(int i = 0;i<arr.length;i++)
        {
            if(i%2!=0)
                sum+=arr[i];
        }

        return sum;
    }

    public static int findMaxIndex(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int max, maxIndex = 0;

        max = arr[0];
        for(int i=1;i< arr.length;i++)
        {
            if(arr[i]>max) {
                max = arr[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    public static int findMinIndex(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int min, minIndex=0;

        min = arr[0];
        for(int i=1;i<arr.length;i++)
        {
            if(arr[i]<min) {
                min = arr[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    public static int findMax(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int max;

        max = arr[0];
        for(int i=1;i<arr.length;i++)
        {
            if(arr[i]>max)
                max=arr[i];
        }
        return max;
    }

    public static int findMin(int el1, int el2, int el3, int el4, int el5)
    {
        int [] arr = new int[]{el1, el2, el3, el4, el5};
        int min;

        min = arr[0];
        for(int i=1;i< arr.length;i++)
        {
            if(arr[i]<min)
                min=arr[i];
        }
        return min;
    }

    public static String showMirror(int num)
    {
        String str = "";
        while(num != 0){
            str += (num % 10);
            num/=10;
        }
        return str;
    }

    public static int sumNumber(int num)
    {
        int sum=0;
        while(num != 0){
            sum += (num % 10);
            num/=10;
        }
        return sum;
    }

    public static int getRoot(int numb)
    {
        int newNumb = numb;
        if(newNumb<0)
        {
            newNumb = -newNumb;
        }
        if(newNumb == 0)
        {
            return 0;
        } else
            if(newNumb == 1){
                return 1;
            }
            double low = 0;
            double high = newNumb;
            double mid = (high-low)/2;

            while(Math.abs((mid*mid)-newNumb)>1.0)
            {
                if((mid*mid)>newNumb)
                {
                    high = mid;
                    mid = low + (high - low) / 2;
                }
                else
                {
                    low = mid;
                    mid = mid + ((high - low)/2);
                }
            }
            if(numb < 0){
                return -(int)Math.round(mid);
            }
            return (int)Math.round(mid);
    }

    public static int getFactorial(int n)
    {
        int mult=1;
        if(n < 0){
            return 0;
        }
        if(n!=0) {
            for (int i = 1; i < n + 1; i++)
                mult*=i;
        }
        return mult;
    }

    public static String checkPrimeNumber(int numb)
    {
        boolean flag = true;
        if(numb == 0||numb == 1)
            flag = false;
        for(int i=2;i<numb;i++)
        {
            if(numb%i==0)
            {
                flag = false;
                break;
            }
        }
        if(flag)
            return "Your number is prime";
        else
            return "Your number isn't prime";
    }

    public static String getSumAndQuantity()
    {
        byte count=0;
        int sum=0;
        for(int i=1;i<100;i++)
        {
            if(i%2==0)
            {
                sum=sum+i;
                count++;
            }
        }
        System.out.println("Sum: "+sum);
        System.out.println("Quantity: "+count);
        return "Sum: " + sum + " Quantity: " + count;
    }

    public static String getStudentRate(int r)
    {
        String sr = "";
            if(r<0||r>100)
                return "Error! Enter a number between 0 and 100 next time.";
            else if(r>=0 && r<=19)
                sr += 'F';
            else if(r>=20 && r<=39)
                    sr += 'E';
            else if(r>=40 && r<=59)
                sr += 'D';
            else if(r>=60 && r<=74)
                sr += 'C';
            else if(r>=75 && r<=89)
                sr += 'B';
            else if(r>=90 && r<=100)
                sr += 'A';
        return sr;
    }

    public static int getMaxExpression(int a, int b, int c)
    {
        return (Math.max(a*b*c,a+b+c))+3;
//        int maxS = (Math.max(a*b*c,a+b+c))+3;
//        System.out.println(maxS);
    }

    public static int getSumOrMultipli(int a, int b)
    {
        int sum;
        if(a == 0){
            return 0;
        }
            if ((a % 2) == 0)
                sum = a * b;
            else
                sum = a + b;

        return sum;
    }
    public static String getQuarter(int x, int y)
    {
        if(x==0)
        {
            if (y>0)
                return "The point is between the first and second quarter";
            else if (y<0)
                return "The point is between the third and fourth quarter";
            else
                return "The point is in the center";
        }
        else if(x>0)
        {
            if(y>0)
                return "The point is in the first quarter";
            else if(y<0)
                return "The point is in the fourth quarter";
            else
                return "The point is between the first and fourth quarter";
        }
        else {
            if(y>0)
                return "The point is in the second quarter";
            else if(y<0)
                return "The point is in the third quarter";
            else
                return "The point is between the second and third quarter";
        }
    }
    public static int positiveSum(int a1, int a2, int a3)
    {
        Scanner str = new Scanner(System.in);
        int sum=0;
        if(a1>0)
            sum=sum+a1;
        if(a2>0)
            sum=sum+a2;
        if(a3>0)
            sum=sum+a3;

        return sum;
    }
}
