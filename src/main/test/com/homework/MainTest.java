package main.test.com.homework;

import main.java.com.homework.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MainTest {

    static List<Arguments> positiveSumTestArgs() {
        return List.of(
                Arguments.arguments(3, 2, 1, 6),
                Arguments.arguments(3, -2, 1, 4),
                Arguments.arguments(-3, 2, -1, 2),
                Arguments.arguments(-3, -2, -1, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("positiveSumTestArgs")
    void positiveSumTest(int a1, int a2, int a3, int expected) {
        int actual = Main.positiveSum(a1, a2, a3);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getQuarterTestArgs() {
        return List.of(
                Arguments.arguments(0, 1, "The point is between the first and second quarter"),
                Arguments.arguments(0, -1, "The point is between the third and fourth quarter"),
                Arguments.arguments(0, 0, "The point is in the center"),
                Arguments.arguments(1, 1, "The point is in the first quarter"),
                Arguments.arguments(1, -1, "The point is in the fourth quarter"),
                Arguments.arguments(1, 0, "The point is between the first and fourth quarter"),
                Arguments.arguments(-1, 1, "The point is in the second quarter"),
                Arguments.arguments(-1, -1, "The point is in the third quarter"),
                Arguments.arguments(-1, 0, "The point is between the second and third quarter")
        );
    }

    @ParameterizedTest
    @MethodSource("getQuarterTestArgs")
    void getQuarterTest(int x, int y, String expected) {
        String actual = Main.getQuarter(x, y);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getSumOrMultipliTestArgs() {
        return List.of(
                Arguments.arguments(2, 2, 4),
                Arguments.arguments(3, 2, 5),
                Arguments.arguments(0, 5, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("getSumOrMultipliTestArgs")
    void getSumOrMultipliTest(int a, int b, int expected) {
        int actual = Main.getSumOrMultipli(a, b);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getMaxExpressionTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 9),
                Arguments.arguments(0, 0, 0, 3),
                Arguments.arguments(-1, -5, -8, -11),
                Arguments.arguments(2, 3, 4, 27)
        );
    }

    @ParameterizedTest
    @MethodSource("getMaxExpressionTestArgs")
    void getMaxExpressionTest(int a, int b, int c, int expected) {
        int actual = Main.getMaxExpression(a, b, c);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getStudentRateTestArgs() {
        return List.of(
                Arguments.arguments(55, "D"),
                Arguments.arguments(100, "A"),
                Arguments.arguments(0, "F"),
                Arguments.arguments(-1, "Error! Enter a number between 0 and 100 next time."),
                Arguments.arguments(101, "Error! Enter a number between 0 and 100 next time.")
        );
    }

    @ParameterizedTest
    @MethodSource("getStudentRateTestArgs")
    void getStudentRateTest(int a, String expected) {
        String actual = Main.getStudentRate(a);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getSumAndQuantityTestArgs() {
        return List.of(
                Arguments.arguments("Sum: 2450 Quantity: 49")
        );
    }

    @ParameterizedTest
    @MethodSource("getSumAndQuantityTestArgs")
    void getSumAndQuantityTest(String expected) {
        String actual = Main.getSumAndQuantity();

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> checkPrimeNumberTestArgs() {
        return List.of(
                Arguments.arguments(3, "Your number is prime"),
                Arguments.arguments(10, "Your number isn't prime")
        );
    }

    @ParameterizedTest
    @MethodSource("checkPrimeNumberTestArgs")
    void checkPrimeNumberTest(int numb, String expected) {
        String actual = Main.checkPrimeNumber(numb);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getFactorialTestArgs() {
        return List.of(
                Arguments.arguments(0, 1),
                Arguments.arguments(10, 3628800),
                Arguments.arguments(-1, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("getFactorialTestArgs")
    void getFactorialTest(int numb, int expected) {
        int actual = Main.getFactorial(numb);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getRootTestArgs() {
        return List.of(
                Arguments.arguments(0, 0),
                Arguments.arguments(1, 1),
                Arguments.arguments(9, 3),
                Arguments.arguments(17, 4),
                Arguments.arguments(-18, -4)
        );
    }

    @ParameterizedTest
    @MethodSource("getRootTestArgs")
    void getRootTest(int numb, int expected) {
        int actual = Main.getRoot(numb);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> sumNumberTestArgs() {
        return List.of(
                Arguments.arguments(10, 1),
                Arguments.arguments(23, 5),
                Arguments.arguments(123, 6),
                Arguments.arguments(-12, -3),
                Arguments.arguments(0, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("sumNumberTestArgs")
    void  sumNumberTest(int num, int expected) {
        int actual = Main.sumNumber(num);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> showMirrorTestArgs() {
        return List.of(
                Arguments.arguments(10, "01"),
                Arguments.arguments(23, "32"),
                Arguments.arguments(123, "321"),
                Arguments.arguments(5, "5")
        );
    }

    @ParameterizedTest
    @MethodSource("showMirrorTestArgs")
    void  showMirrorTest(int num, String expected) {
        String actual = Main.showMirror(num);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> findMinTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, 1),
                Arguments.arguments(5, 4, 3, 2, 1, 1),
                Arguments.arguments(0, -1, 5, -5, 3, -5),
                Arguments.arguments(-2, -4, -5, 4, -8, -8)
        );
    }

    @ParameterizedTest
    @MethodSource("findMinTestArgs")
    void  findMinTest(int el1, int el2, int el3, int el4, int el5, int expected) {
        int actual = Main.findMin(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> findMaxTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, 5),
                Arguments.arguments(5, 4, 3, 2, 1, 5),
                Arguments.arguments(0, -1, -5, -5, -3, 0),
                Arguments.arguments(-2, -4, -5, -4, -8, -2)
        );
    }

    @ParameterizedTest
    @MethodSource("findMaxTestArgs")
    void  findMaxTest(int el1, int el2, int el3, int el4, int el5, int expected) {
        int actual = Main.findMax(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> findMinIndexTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, 0),
                Arguments.arguments(5, 4, 3, 2, 1, 4),
                Arguments.arguments(0, -1, 5, -5, 3, 3),
                Arguments.arguments(-2, -4, -5, 4, -8, 4)
        );
    }

    @ParameterizedTest
    @MethodSource("findMinIndexTestArgs")
    void  findMinIndexTest(int el1, int el2, int el3, int el4, int el5, int expected) {
        int actual = Main.findMinIndex(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> findMaxIndexTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, 4),
                Arguments.arguments(5, 4, 3, 2, 1, 0),
                Arguments.arguments(0, -1, -5, -5, -3, 0),
                Arguments.arguments(-2, -4, -5, 4, -8, 3)
        );
    }

    @ParameterizedTest
    @MethodSource("findMaxIndexTestArgs")
    void  findMaxIndexTest(int el1, int el2, int el3, int el4, int el5, int expected) {
        int actual = Main.findMaxIndex(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> reverseArrTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, "5 4 3 2 1 "),
                Arguments.arguments(5, 4, 3, 2, 1, "1 2 3 4 5 "),
                Arguments.arguments(0, -1, -5, -5, -3, "-3 -5 -5 -1 0 ")
        );
    }

    @ParameterizedTest
    @MethodSource("reverseArrTestArgs")
    void  reverseArrTest(int el1, int el2, int el3, int el4, int el5, String expected) {
        String actual = Main.reverseArr(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> sumOddElemTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, 6),
                Arguments.arguments(5, 4, 3, 2, 1, 6),
                Arguments.arguments(0, -1, -5, -5, -3, -6),
                Arguments.arguments(0, 0, -5, 0, -3, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("sumOddElemTestArgs")
    void  sumOddElemTest(int el1, int el2, int el3, int el4, int el5, int expected) {
        int actual = Main.sumOddElem(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getOddNumsQuantityTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, 3),
                Arguments.arguments(5, 4, 3, 2, 1, 3),
                Arguments.arguments(0, -1, -5, -5, -3, 4),
                Arguments.arguments(0, 0, -5, 0, -3, 2)
        );
    }

    @ParameterizedTest
    @MethodSource("getOddNumsQuantityTestArgs")
    void  getOddNumsQuantityTest(int el1, int el2, int el3, int el4, int el5, int expected) {
        int actual = Main.getOddNumsQuantity(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> swapHalvesTestArgs() {
        return List.of(
                Arguments.arguments(1, 2, 3, 4, 5, "4 5 3 1 2 "),
                Arguments.arguments(5, 4, 3, 2, 1, "2 1 3 5 4 "),
                Arguments.arguments(0, -1, -5, -5, -3, "-5 -3 -5 0 -1 ")
        );
    }

    @ParameterizedTest
    @MethodSource("swapHalvesTestArgs")
    void  swapHalvesTest(int el1, int el2, int el3, int el4, int el5, String expected) {
        String actual = Main.swapHalves(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> sortBubbleTestArgs() {
        return List.of(
                Arguments.arguments(-3, 2, 0, -1, 5, "-3 -1 0 2 5 "),
                Arguments.arguments(5, 4, 3, 2, 1, "1 2 3 4 5 "),
                Arguments.arguments(0, 0, 0, 3, -5, "-5 0 0 0 3 ")
        );
    }

    @ParameterizedTest
    @MethodSource("sortBubbleTestArgs")
    void  sortBubbleTest(int el1, int el2, int el3, int el4, int el5, String expected) {
        String actual = Main.sortBubble(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> sortSelectTestArgs() {
        return List.of(
                Arguments.arguments(1, 5, 2, 4, 3, "1 2 3 4 5 "),
                Arguments.arguments(5, 4, 3, 2, 1, "1 2 3 4 5 "),
                Arguments.arguments(0, -8, 1, -5, 3, "-8 -5 0 1 3 ")
        );
    }

    @ParameterizedTest
    @MethodSource("sortSelectTestArgs")
    void  sortSelectTest(int el1, int el2, int el3, int el4, int el5, String expected) {
        String actual = Main.sortSelect(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> sortInsertTestArgs() {
        return List.of(
                Arguments.arguments(8, 3, 1, 5, 7, "1 3 5 7 8 "),
                Arguments.arguments(5, 4, 3, 2, 1, "1 2 3 4 5 "),
                Arguments.arguments(0, -1, -5, -3, -8, "-8 -5 -3 -1 0 ")
        );
    }

    @ParameterizedTest
    @MethodSource("sortInsertTestArgs")
    void  sortInsertTest(int el1, int el2, int el3, int el4, int el5, String expected) {
        String actual = Main.sortInsert(el1, el2, el3, el4, el5);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getDayTestArgs() {
        return List.of(
                Arguments.arguments(1, "Monday"),
                Arguments.arguments(5, "Friday"),
                Arguments.arguments(0, "Enter a number between 1 and 7")
        );
    }

    @ParameterizedTest
    @MethodSource("getDayTestArgs")
    void  getDayTest(int day, String expected) {
        String actual = Main.getDay(day);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getDotDistTestArgs() {
        return List.of(
                Arguments.arguments(1, 1, -1, -1, "Distance between two dots is: 1.4142135623730951"),
                Arguments.arguments(5, -1, 0, 3, "Distance between two dots is: 3.2015621187164243"),
                Arguments.arguments(0, 0, 0, 0, "Distance between two dots is: 0.0")
        );
    }

    @ParameterizedTest
    @MethodSource("getDotDistTestArgs")
    void  getDotDistTest(int x1, int y1, int x2, int y2, String expected) {
        String actual = Main.getDotDist(x1, y1, x2, y2);

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> getStrTestArgs() {
        return List.of(
                Arguments.arguments(324, "триста двадцать четыре"),
                Arguments.arguments(25, "двадцать пять"),
                Arguments.arguments(7, "семь"),
                Arguments.arguments(0, "ноль"),
                Arguments.arguments(1000, "Invalid number! Enter a number between 0 and 999...")
        );
    }

    @ParameterizedTest
    @MethodSource("getStrTestArgs")
    void  getStrTest(int num, String expected) {
        String actual = Main.getStr(num);

        Assertions.assertEquals(expected, actual);
    }
}
